<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Exmaple Routes
Route::get('/index', 'HomeController@index');
Route::get('/examples', 'HomeController@examples');
Route::get('/blog', 'HomeController@blog');
Route::get('/super99', 'HomeController@super99');

//Platform Routes
Route::get('/inicio', 'BaseController@inicio');
Route::get('/como_decido_ser_super/documentos', 'BaseController@documentos');
Route::get('/como_decido_ser_super/super_serie', 'BaseController@super_serie');
Route::get('/como_decido_ser_super/guia_basica_para_ser_super', 'BaseController@guia_basica_para_ser_super');
Route::get('/como_decido_ser_super/guia_basica_para_ser_super', 'BaseController@guia_basica_para_ser_super');
Route::get('/como_decido_ser_super/codigo_de_etica', 'BaseController@codigo_de_etica');
Route::get('/como_decido_ser_super/codigo_de_etica_ver/{id}', 'BaseController@codigo_de_etica_ver');
Route::get('/como_decido_ser_super/manual_y_politica_anticorrupcion', 'BaseController@manual_y_politica_anticorrupcion');
Route::get('/como_decido_ser_super/codigo_de_buen_gobierno', 'BaseController@codigo_de_buen_gobierno');

//Admin Routes
Route::get('/admin/list_contents', 'AdminController@list_contents');
Route::get('/admin/create_content', 'AdminController@create_content');
Route::get('/admin/edit_content/{id}', 'AdminController@edit_content');

Route::post('/admin/update_content', 'AdminController@update_content');
Route::post('/admin/store_content', 'AdminController@store_content');
Route::post('/admin/delete_content', 'AdminController@delete_content');

Route::get('/', function () {
    return redirect("/inicio");
});
