
@extends('layout')

@section('content')
	
	<h1>List Contents</h1>
	
	<p>
		<select id="select_category" name="category">
		  <option value="">Select Category</option>
	  	  <option value="codigo_de_etica">codigo_de_etica</option>
	  	  <option value="manual_y_politica_anticorrupcion">manual_y_politica_anticorrupcion</option>
		  <option value="codigo_de_buen_gobierno">codigo_de_buen_gobierno</option>
	  	  <option value="super_serie">super_serie</option>

	</select>
	</p>
	
	<a href="/admin/create_content">Create Content</a>

    <div class="row">
        <div class="col-md-12">
            @if($contents->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
							
                            <th>ID</th>
                            <th>Title</th>
							<th>Category</th>
							<th>Body</th>
							<th>Video Link</th>
							
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($contents as $content)
                            <tr>
                                <td>{{$content->id}}</td>
                                <td>{{$content->title}}</td>
								<td>{{$content->category}}</td>
								<td>{{$content->body}}</td>
								<td>{{$content->video_link}}</td>
								<td>
								
								</td>
                                <td class="text-right">
                                    
                                    <a href="/admin/edit_content/{{$content->id}}" class="btn btn-xs btn-warning">Edit</a>
									
                                    <form action="/admin/delete_content" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="content_id" value="{{$content->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
	
<script>
	
	$( document ).ready(function() {
	    
	});
	
	$( "#select_category" ).change(function() {
	 	// alert( "Handler for .change() called." + $(this).val());
		
		if($(this).val() !=""){
			window.location.assign("/admin/list_contents?category="+$(this).val())
		}
		  
	});
	
</script>

	      
@endsection