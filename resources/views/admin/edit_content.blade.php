@extends('layout')


@section('content')
  
    <div class="row">
        <div class="col-md-12">

            <form action="/admin/update_content" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="content_id" value="{{$content->id}}">
				
                <label>Category</label>
                <input type="text" id="category-theme_url" name="category" class="form-control" value="{{ $content->category }}"/>
				<br />

                <label>Title</label>
                <input type="text" id="title-field" name="title" class="form-control" value="{{$content->title}}"/>
				<br />
				
				<textarea rows="4" cols="50" name="body" id="body-field">
				{{$content->body}}
				</textarea>
                <br />
				
                <label>Video Link</label>
                <input type="text" id="video_link-field" name="video_link" class="form-control" value="{{$content->video_link}}"/>
				<br />
			
				
				<input type="submit" class="et_manage_submit create_barsite" value="Save">
				
                
            </form>

        </div>
    </div>
@endsection