@extends('layout')


@section('content')
  
    <div class="row">
        <div class="col-md-12">

            <form action="/admin/store_content" method="POST" enctype="multipart/form-data">
				
				
				<p>
					<select id="select_category" name="category">
				  	  <option value="codigo_de_etica">codigo_de_etica</option>
				  	  <option value="manual_y_politica_anticorrupcion">manual_y_politica_anticorrupcion</option>
				  	  <option value="super_serie">super_serie</option>
				</select>
				</p>
        
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <label>Title</label>
                <input type="text" id="title-field" name="title" class="form-control" value=""/>
				<br />
				
				<label>Body</label><br />
				<textarea rows="4" cols="50" name="body" id="body-field"></textarea>
                <br />
				
                <label>Video Link</label>
                <input type="text" id="video_link-field" name="video_link" class="form-control" value=""/>
				<br />
			
				
				<input type="submit" class="et_manage_submit create_barsite" value="Save">
				
                
            </form>

        </div>
    </div>
@endsection