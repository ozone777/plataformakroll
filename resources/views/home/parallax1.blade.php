<html>

<style>

	body{
	  margin: 0;
	  background-color: #1d1e22;
	}
	#parallax {
	  position: relative;
	  width: 100%;
	  height: 100vh;
	  background-image: url(https://raw.githubusercontent.com/oscicen/oscicen.github.io/master/img/depth-3.png), url(https://raw.githubusercontent.com/oscicen/oscicen.github.io/master/img/depth-2.png), url(https://raw.githubusercontent.com/oscicen/oscicen.github.io/master/img/depth-1.png);
	  background-repeat: no-repeat;
	  background-position: center;
	  background-position: 50% 50%;
	}
	h1 {
	  position: absolute;
	  top: 47%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  color: #fff;
	  font-family: "Arial";
	  text-transform: uppercase;
	  opacity: .2;
	  font-size: 70px;
	}
	
</style>

<header><title>Platform Examples</title></header>
<body>
	
	<div style="color:#FFF" id="parallax"><h1>Parallax</h1>
  
	  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

	Why do we use it?
	It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


	Where does it come from?
	Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in </p>
  
	</div>
	

<script>
	
	(function() {
	    // Add event listener
	    document.addEventListener("mousemove", parallax);
	    const elem = document.querySelector("#parallax");
	    // Magic happens here
	    function parallax(e) {
	        let _w = window.innerWidth/2;
	        let _h = window.innerHeight/2;
	        let _mouseX = e.clientX;
	        let _mouseY = e.clientY;
	        let _depth1 = `${50 - (_mouseX - _w) * 0.01}% ${50 - (_mouseY - _h) * 0.01}%`;
	        let _depth2 = `${50 - (_mouseX - _w) * 0.02}% ${50 - (_mouseY - _h) * 0.02}%`;
	        let _depth3 = `${50 - (_mouseX - _w) * 0.06}% ${50 - (_mouseY - _h) * 0.06}%`;
	        let x = `${_depth3}, ${_depth2}, ${_depth1}`;
	        console.log(x);
	        elem.style.backgroundPosition = x;
	    }

	})();
	
</script>
	
</body>
</html>