<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Blog Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link href="/css/blog.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
      
    <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main">
          <h1 class="pb-3 mb-4 font-italic border-bottom">
            Ejemplos
          </h1>

          <div class="blog-post">
            <h2 class="blog-post-title">Lineas de Tiempo</h2>
           
			<ul id="menu">
			    <a href="/examples?example=timeline1"><li>Timeline 1</li></a>
			    <a href="http://preview.codecanyon.net/item/first-responsive-timeline/full_screen_preview/19352502?_ga=2.215112849.741595352.1565192615-2145745231.1564007009"><li>Timeline 2</li></a>
			    <a href="https://www.cssscript.com/demo/responsive-horizontal-vertical-timeline/"><li>Timeline 3</li></a>
			    <a href="https://www.cssscript.com/demo/responsive-timeline-slider-javascript-css/"><li>Timeline 4</li></a>
				<a href="https://www.cssscript.com/demo/responsive-animated-timeline-javascript-css3/"><li>Timeline 5</li></a>
			</ul>
           
          </div><!-- /.blog-post -->

          <div class="blog-post">
            <h2 class="blog-post-title">Textos interactivos</h2>
            
			<ul id="menu">
		
		
			<a href="https://codecanyon.net/item/interactive-3d-flip-book-with-physicsbased-animation-jquery-plugin/19113743"><li>Interactive 1</li></a>
				<a href="https://mozilla.github.io/pdf.js/web/viewer.html"><li>PDF Viewer</li></a>
			    <a href="/examples/?example=parallax1"><li>Parallax1</li></a>
				<a href="https://templatemo.com/live/templatemo_532_next_level"><li>Parallax4</li></a>
				<a href="https://templatemo.com/live/templatemo_534_parallo"><li>Parallax5</li></a>
				<a href="https://templatemo.com/live/templatemo_467_easy_profile"><li>Parallax6</li></a>
		
			</ul>
			
			</div><!-- /.blog-post -->
			
        </div><!-- /.blog-main -->

      
      </div><!-- /.row -->

    </main><!-- /.container -->

    <footer class="blog-footer">
      <p>Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/js/jquery-slim.min.js"><\/script>')</script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  </body>
</html>
