<html>
<header>
	<title>Super99</title>
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
	<style>
	
		.dropdown:hover>.dropdown-menu{
			display: block;
		}
		
		.dropdown-item{
			 text-transform: uppercase;
		}
		
		.nav-link{
			text-transform: uppercase;
		}
		
	</style>
	
</header>
<body>


	
	  <div class="row">
	    <div class="col-sm">
		  <img src="/images/iconSuper99.png" alt="Smiley face" height="auto" width="100">
	      
	    </div>
	    <div class="col-sm">
	      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
	      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
	    </div>
	  </div>
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <a class="navbar-brand" href="#"></a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	    
  	      <li class="nav-item dropdown">
  	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  	          ¿Cómo decido ser súper?
  	        </a>
  	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
  	          <a class="dropdown-item" href="#">documentos</a>
  	          <a class="dropdown-item" href="#">súper serie (videos)</a>  
  	          <a class="dropdown-item" href="#">Guía básica para ser súper</a>
  	        </div>
  	      </li>
		  
  	      <li class="nav-item dropdown">
  	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  	          recursos del cambio
  	        </a>
  	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
  	          <a class="dropdown-item" href="#">fases del cambio</a>
  	          <a class="dropdown-item" href="#">revista 99</a>
  	          <a class="dropdown-item" href="#">historias del cambio</a>
			  <a class="dropdown-item" href="#">detrás de cámaras</a>
  	        </div>
  	      </li>
		  
  	      <li class="nav-item dropdown">
  	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  	          espacio de capacitaciones
  	        </a>
  	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
  	          <a class="dropdown-item" href="#">espAcio webinars / <br />capacitaciones</a>
  	          <a class="dropdown-item" href="#">test de conocimiento</a>
  	          <a class="dropdown-item" href="#">repositorio presentaciones
y<br /> capacitaciones anteriores</a>
  	        </div>
  	      </li>	  
	
  	    </ul>

	    
	  </div>
	</nav>

	
	<div class="container">
	  <div class="row">
	   <div class="col">
			<div class="text-center">
		  <img src="/images/man.png" height="400px">
	      </div>
	    </div>
	    <div class="col">
		
			<div class="text-center" style="margin-top: 70px">
		  <img src="/images/brandone.png" width="250px" style="margin-top: 30px">
	      <p style="text-align: center; margin-top: 30px">SUPER99 <br /> está apuntando hacia <br /> una filosofía de integridad <br />y ética empresarial.</p>
	  	  </div>
		  
	    </div>
	    <div class="col">
	     
	    </div>
	  </div>
	  
	  
  	<div class="row">
  	    <div class="col" style="background-color: #F5333F">
  			<div class="text-center">
  	      	  <h1 style="text-transform: uppercase; color:#FFF">nuestras <br />
				  historias<br />
				  del cambio</h1>
  			 </div>
  	    </div>
  	    <div class="col">
  			<div class="text-center">
  	     	   <img src="/images/homevideo.png" width="100%">
  		  </div>
  	    </div>
  	  </div>
	  
	  
	  
	  <div class="row" style="background-color: #0D96D3; color: #FFF">
		  
	      <div class="col">
			  <div class="text-center">
	       	  	 <h1>1º</h1>
				 <p>SUPERMERCADO EN PANAMA <br />
EN EL DISEÑO E IMPLEMENTACIÓN<br />
DE UN PROGRAMA DE CUMPLIENTO<br />
ANTICORRUPCIÓN</p>
			  </div>
	      </div>
		  
	      <div class="col">
			  <div class="text-center">
	        	  <h1>7k</h1>
				  <p>COLABORADORES<br />
CAPACITADOS SOBRE<br />
BUEN GOBIERNO</p>
			  </div>
	      </div>
		  
	      <div class="col">
			  <div class="text-center">
	       	   	<h1>200</h1>
				<p>POLÍTICAS Y<br />
PROCEDIMIENTOS<br />
CLAROS Y TRANSPARENTE</p>
		   	  </div>
	      </div>
		  
	      <div class="col">
			  <div class="text-center">
			  	<img src="/images/checksign.png" width="77">
	          </div>
			  <p>
				GENERACIÓN DE<br />
				CONFIANZA EN EL <br />
				SECTOR FINANCIERO
			  </p>
	      </div>
		  
	    </div>
	  
	  
      <div class="row">
         <div class="col">
           	<img src="/images/homeicon1.png" width="100%">
         </div>
         <div class="col">
           	<img src="/images/homeicon2.png" width="100%">
         </div>
         <div class="col">
          	<img src="/images/homeicon3.png" width="100%">
         </div>
       </div>
	   
	   
	 
	  
	  
	</div>
	
	
	<footer class="footer" style="background-color: #F5333F;">
	      <div class="container" >
			  <div class="text-center">
	        	  <p style="color: #FFF; padding: 20px 20px 20px 20px;">COPYRIGHT . SUPER99 KROLL 2019 . todos los derechos reservados . DISEÑO MILPAGROUP.COM</p>
			  </div>
	      </div>
	  </footer></body>
</html>