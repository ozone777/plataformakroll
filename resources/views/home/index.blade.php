<html>
<header>
	
	<title>Platform Examples</title>


    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    

</header>
<body style="margin: 20px 20px 20px 20px">
	
	<h1 syle="margin-text: center">Ejemplos para plataforma KROLL</h1>
	
	<h2>Lineas de Tiempo</h2>
	
	<ul id="menu">
	    <a href="/examples?example=timeline1"><li>Timeline 1</li></a>
	    <a href="http://preview.codecanyon.net/item/first-responsive-timeline/full_screen_preview/19352502?_ga=2.215112849.741595352.1565192615-2145745231.1564007009"><li>Timeline 2</li></a>
	    <a href="https://www.cssscript.com/demo/responsive-horizontal-vertical-timeline/"><li>Timeline 3</li></a>
	    <a href="https://www.cssscript.com/demo/responsive-timeline-slider-javascript-css/"><li>Timeline 4</li></a>
		<a href="https://www.cssscript.com/demo/responsive-animated-timeline-javascript-css3/"><li>Timeline 5</li></a>
	</ul>
	

	
	<h2>Textos y Videos Interactivos</h2>
	
	<ul id="menu">
		
		
	<a href="https://codecanyon.net/item/interactive-3d-flip-book-with-physicsbased-animation-jquery-plugin/19113743"><li>Interactive 1</li></a>
		<a href="https://mozilla.github.io/pdf.js/web/viewer.html"><li>PDF Viewer</li></a>
	    <a href="/examples/?example=parallax1"><li>Parallax1</li></a>
		<a href="https://templatemo.com/live/templatemo_532_next_level"><li>Parallax4</li></a>
		<a href="https://templatemo.com/live/templatemo_534_parallo"><li>Parallax5</li></a>
		<a href="https://templatemo.com/live/templatemo_467_easy_profile"><li>Parallax6</li></a>
		
	</ul>
	
	
	
	<h2>Organigramas</h2>
	
	<ul id="menu">

		<a href="/examples/?example=org"><li>Organigrama</li></a>
	 	<a href="/examples/?example=org2"><li>Organigrama2</li></a>
		
	</ul>
	
	<h2>Webinars</h2>
	
	<ul id="menu">
		<a href="https://www.lawebera.es/promocionar/como-hacer-webinar-youtube-live.php"><li>Webinars</li></a>
		
	</ul>
</body>
</html>