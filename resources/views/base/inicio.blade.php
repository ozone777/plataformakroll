@extends('layout')

@section('content')
	
	  <div class="row">
	   <div class="col">
			<div class="text-center">
		  <img src="/images/man.png" height="400px">
	      </div>
	    </div>
	    <div class="col">
		
			<div class="text-center" style="margin-top: 70px">
		  <img src="/images/brandone.png" width="250px" style="margin-top: 30px">
	      <p style="text-align: center; margin-top: 30px">SUPER99 <br /> está apuntando hacia <br /> una filosofía de integridad <br />y ética empresarial.</p>
	  	  </div>
		  
	    </div>
	    <div class="col">
	     
	    </div>
	  </div>
	  
	  
  	<div class="row">
  	    <div class="col" style="background-color: #F5333F">
  			<div class="text-center">
  	      	  <h1 style="text-transform: uppercase; color:#FFF">nuestras <br />
				  historias<br />
				  del cambio</h1>
  			 </div>
  	    </div>
  	    <div class="col">
  			<div class="text-center">
  	     	   <img src="/images/homevideo.png" width="100%">
  		  </div>
  	    </div>
  	  </div>
	  
	  
	  
	  <div class="row" style="background-color: #0D96D3; color: #FFF">
		  
	      <div class="col">
			  <div class="text-center">
	       	  	 <h1>1º</h1>
				 <p>SUPERMERCADO EN PANAMA <br />
EN EL DISEÑO E IMPLEMENTACIÓN<br />
DE UN PROGRAMA DE CUMPLIENTO<br />
ANTICORRUPCIÓN</p>
			  </div>
	      </div>
		  
	      <div class="col">
			  <div class="text-center">
	        	  <h1>7k</h1>
				  <p>COLABORADORES<br />
CAPACITADOS SOBRE<br />
BUEN GOBIERNO</p>
			  </div>
	      </div>
		  
	      <div class="col">
			  <div class="text-center">
	       	   	<h1>200</h1>
				<p>POLÍTICAS Y<br />
PROCEDIMIENTOS<br />
CLAROS Y TRANSPARENTE</p>
		   	  </div>
	      </div>
		  
	      <div class="col">
			  <div class="text-center">
			  	<img src="/images/checksign.png" width="77">
	          </div>
			  <p>
				GENERACIÓN DE<br />
				CONFIANZA EN EL <br />
				SECTOR FINANCIERO
			  </p>
	      </div>
		  
	    </div>
	  
	  
      <div class="row">
         <div class="col">
           	<img src="/images/homeicon1.png" width="100%">
         </div>
         <div class="col">
           	<img src="/images/homeicon2.png" width="100%">
         </div>
         <div class="col">
          	<img src="/images/homeicon3.png" width="100%">
         </div>
       </div>
	   
	   
@endsection