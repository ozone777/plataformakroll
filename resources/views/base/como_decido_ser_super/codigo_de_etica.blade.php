@extends('layout')


@section('content')
  
  <h1>Codigo de Etica</h1>
  
  
  <div class="row">
	
     <div class="col">
		 
		 <div class="text-center">
	   <h3>tabla de contenido</h3>
	   
	   <ul>
	   @foreach($contents as $content)
  
	   	<li style="text-align: left"><a href="/como_decido_ser_super/codigo_de_etica_ver/{{$content->id}}">{{$content->title}}</li>
  	
	   @endforeach
	 	</ul>
	   
	   </div>
	   
     </div>
	
	
     <div class="col">
	   	 <div class="text-center">
       	<img src="/images/conoce_super_etica.jpg" >
	</div>
     </div>
   
  </div>
  
 
	
@endsection