@extends('layout')

@section('content')
	
	@include('base/como_decido_ser_super/_header')
	
    <div class="row">
		
       <div class="col" style="color: #F5333F">
		   <div class="text-center">
         	<h3>¿CÓMO DECIDO SER SÚPER?</h3>
			<h1>Documentos</h1>
			 </div>
       </div>
      
     </div>
	 
	 
	 
     <div class="row">
		
        <div class="col">
 		   <div class="text-right">
          		<a href="/como_decido_ser_super/codigo_de_etica"><img src="/images/codigo_etica_icon.png" width="300"></a><br />
				<a href="/como_decido_ser_super/manual_y_politica_anticorrupcion"><img src="/images/manual_y_politica_icon.png" width="300"></a><br />
				<a href="/como_decido_ser_super/codigo_de_buen_gobierno"><img src="/images/codigo_de_gobierno.png" width="300"></a> <br />
				<img src="/images/documentos_confidenciales.png" width="300"> <br />
 			 </div>
        </div>
		
		
        <div class="col">
 		   
          	<img src="/images/mujerimagen.png" height="700">
 		  
        </div>
      
     </div>
	

	      
@endsection