<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;

class initialize_contents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initialize_contents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize contents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

		DB::table('contents')->delete();
		
		DB::table('contents')->insert(['title' => 'Declaración de la Junta Directiva ', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Objetivo del Código de Ética', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Pilares éticos', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Pautas éticas y de conducta', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Manejo de la información del Grupo', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Uso adecuado de los recursos de la empresa', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Relaciones transparentes y sostenibles con los grupos de interés', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Ética en los negocios', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Respeto de la libre competencia', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Respeto de los Derechos Humanos', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Igualdad de acceso a las oportunidades', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'laborales y remuneración salarial', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Actos de acoso e intimidación ', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Acoso sexual', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Canales para la manifestación de las preocupaciones y denuncias de los colaboradores (open talk)', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Seguimiento al cumplimiento del Código de Ética', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Incumplimiento de las disposiciones del Código de Ética', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Incumplimiento de las disposiciones del Código de Ética', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Dudas y solicitudes', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('contents')->insert(['title' => 'Dudas y solicitudes', 'body' => '<h1>Hello World</h1>','type' => 'text','category'=>'codigo_de_etica','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		
		
		
		
    }
}
