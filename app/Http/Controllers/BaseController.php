<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Content;
use App\UsersContent;

class BaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
	
	public function inicio(){
		
		return view('base.inicio');
	}
	
	public function documentos(){
		
		return view('base.como_decido_ser_super.documentos');
	}
	
	public function super_serie(){
		
		return view('base.como_decido_ser_super.super_serie');
	}
	
	public function guia_basica_para_ser_super(){
		
		return view('base.como_decido_ser_super.guia_basica_para_ser_super');
	}
	
	public function codigo_de_etica(){
		$contents = Content::where('category',"codigo_de_etica")->get();
		return view('base.como_decido_ser_super.codigo_de_etica',compact('contents'));
	}
	
	public function codigo_de_etica_ver($id){
		$content = Content::findOrFail($id);
		return view('base.como_decido_ser_super.codigo_de_etica_ver',compact('content'));
	}
	
	public function codigo_de_buen_gobierno(){
		
		return view('base.como_decido_ser_super.codigo_de_buen_gobierno');
	}
	
	public function manual_y_politica_anticorrupcion(){
		
		return view('base.como_decido_ser_super.manual_y_politica_anticorrupcion');
	}
	
	
}
