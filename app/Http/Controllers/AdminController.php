<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Content;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
	
	public function list_contents(){
		$category = Input::get('category');
		
		if(isset($category)){
			$contents = Content::orderBy('id', 'desc')->where("category",$category)->get();
		}else{
			$contents = Content::orderBy('id', 'desc')->where("category","codigo_de_etica")->get();
		}
		
		return view('admin.list_contents',compact('contents'));
	}
	
	public function edit_content($id){
		
		$content = Content::findOrFail($id);
		return view('admin.edit_content',compact('content'));
		
	}
	
	public function create_content(){
		
		return view('admin.create_content');
	}
	
	
	public function update_content(){
		
		$content_id = Input::get('content_id');
		$content = Content::findOrFail($content_id);
		
		$content->body = Input::get('body');
		$content->title = Input::get('title');
		$content->video_link = Input::get('video_link');
		$content->category = Input::get('category');
		$content->save();
		
		return redirect("/admin/list_contents?category=".$content->category);
		//return view('admin.list_contents');
	}
	
	public function store_content(){
		
		$content = new Content();
		$content->body = Input::get('body');
		$content->title = Input::get('title');
		$content->video_link = Input::get('video_link');
		$content->category = Input::get('category');
		$content->save();
		
		return redirect("/admin/list_contents?category=".$content->category);
		//return view('admin.list_contents');
	}
	
	
	public function delete_content(){
		
		$content_id = Input::get('content_id');
		$content = Content::findOrFail($content_id);
		
		$content->delete();
		
		return redirect("/admin/list_contents");
		//return view('admin.list_contents');
	}
	
	
}
