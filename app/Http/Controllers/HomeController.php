<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.index');
    }
	
	public function super99(){
		
		return view('home.super99');
	}
	
    public function blog()
    {
        return view('home.blog');
    }
	
    public function examples()
    {
		$example = Input::get('example');
		return view('home.'.$example);
		
    }
	
}
